const HDWalletProvider = require('truffle-hdwallet-provider');
const fs = require('fs');
module.exports = {
  networks: {
    blockchaincons: {
      network_id: "*",
      gas: 0,
      gasPrice: 0,
      provider: new HDWalletProvider(fs.readFileSync('c:\\Users\\Sysgain Inc\\Desktop\\VS-code-to-send-transaction\\mniminic.env', 'utf-8'), "https://blockchainmember.blockchain.azure.com:3200/4qNtwxBlt0hvAONJCFH_z5TU"),
      consortium_id: 1562238188552
    },
    test99cons: {
      network_id: "*",
      gas: 0,
      gasPrice: 0,
      provider: new HDWalletProvider(fs.readFileSync('c:\\Users\\Sysgain Inc\\Documents\\mnimonic.env', 'utf-8'), "https://testblockchain.blockchain.azure.com:3200/G_QAdfg1jk7evd2Hkf1QJNap"),
      consortium_id: 1562766618135
    }
  },
  mocha: {},
  compilers: {
    solc: {
      version: "0.5.0"
    }
  }
};
